-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 14, 2014 at 01:21 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblPages`
--

CREATE TABLE IF NOT EXISTS `tblPages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tblPages`
--

INSERT INTO `tblPages` (`id`, `title`, `body`) VALUES
(1, 'Главная', '<p>Всем привет!</p>\r\n<p>Вот и получилось запустить первую версию сайта. Надеюсь, у вас всё получилось и появились какие-нибудь вопросы.</p>\r\n<p>Вы можете написать мне <a href="mailto:warlockfx@gmail.com">на почту</a> или <a href="https://vk.com/vgafurov" target="_blank">вконтакт</a>.</p>'),
(2, 'Полезные материалы', '<p>Для дальнейшего обучения, вам рекомендуется почитать вот эти ссылки:</p>\r\n<ul>\r\n<li><a href="http://php.net/manual/ru/intro.pdo.php" target="_blank">http://php.net/manual/ru/intro.pdo.php</a></li>\r\n<li><a href="http://ru.wikipedia.org/wiki/PhpDocumentor" target="_blank">http://ru.wikipedia.org/wiki/PhpDocumentor</a></li>\r\n<li><a href="http://habrahabr.ru/post/38214/" target="_blank">http://habrahabr.ru/post/38214/</a></li>\r\n</ul>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
