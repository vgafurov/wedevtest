<?php

spl_autoload_register('loadClass');

function loadClass($className)
{

    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= $className . '.php';    

    $classPath = 'classes/' . DIRECTORY_SEPARATOR . $fileName;
    if (is_file($classPath) && is_readable($classPath)) {
        include($classPath);
        return;
    }
}