<?php

class Comment
{

    static public function fetchAll()
    {
        $dbh = Database::getConnection();

        $sql = "SELECT * FROM `tblComments` ORDER BY `cdate` DESC";
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }

    public function count()
    {
        $dbh = Database::getConnection();

        $sqlCnt = "SELECT count(*) AS `cnt` FROM `tblComments`";
        $stmt = $dbh->prepare($sqlCnt);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['cnt'];
    }

    public function get($page = 1, $perPage = 5)
    {
        if ($page == 0) {
            $page = 1;
        }
        $dbh = Database::getConnection();

        $offset = intval($perPage * ($page - 1));

        $sql = "SELECT * FROM `tblComments` ORDER BY `cdate` DESC LIMIT :off, :perpage";
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(':off', $offset, PDO::PARAM_INT);
        $stmt->bindValue(':perpage', $perPage, PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $rows;

    }

    static public function renderComments($rows)
    {
        $smarty = Template::getInstance();
        $smarty->assign('comments', $rows);

        return $smarty->fetch('comments.tpl');
    }

    static public function save($user, $comment)
    {
        $dbh = Database::getConnection();

        $sql = "INSERT INTO `tblComments` SET `username` = :username, `comment` = :comment, `cdate` = Now()";
        $placeholders = array(
            ':username' => $user,
            ':comment' => $comment,
        );

        $stmt = $dbh->prepare($sql);
        if ($stmt->execute($placeholders)) {
            return 'Комментарий добавлен!';
        } else {
            return 'Произошла ошибка!';
        }
    }

    static public function last()
    {
        // Получение последнего комментария
        $dbh = Database::getConnection();

        $sql = 'SELECT * FROM `tblComments` ORDER BY `cdate` DESC LIMIT 1';
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row;
    }
}