<?php

class Controller
{
    protected $output = array();
    protected $template = 'default';
    protected $tpl = 'index.tpl';

    public function run()
    {
        $this->output['menu_items'] = Page::getMenu();
        $this->output['last_comment'] = Comment::last();

    }

    public function getOutput()
    {
        return $this->output;
    }
    public function getTemplate()
    {
        return $this->template;
    }

    public function render()
    {
        $smarty = Template::getInstance($this->getTemplate());

        if (count($this->output) > 0) {
            foreach ($this->output as $key => $val) {
                $smarty->assign($key, $val);
            }
        }

        $smarty->display($this->tpl);

    }
}