<?php
namespace Controllers;

class Administrator extends \Controller
{
    protected $template = 'admin';
    public function run()
    {
        parent::run();

        session_start();

        $contents = array();

        $action = 'administrator';
        
        $template = 'admin';
        $contents['body'] = 'Панель управления сайтом';

        if (\User::isAuthorized()) {
            if (isset($_GET['action'])) {
                $action = $_GET['action'];
                switch ($action) {
                    case 'logout':
                        \User::doLogout();
                        break;
                    case 'editPage':
                        if (isset($_GET['id'])) {
                            $contents['row'] = \Page::edit($_GET['id']);
                        } else {
                            $contents['row'] = null;
                        }
                        break;
                    case 'addPage':
                        break;
                    case 'listPages':
                        $contents['rows'] = \Page::fetchAll();
                        break;
                    case 'deletePage':
                        if (isset($_GET['id'])) {
                            if (\Page::delete($_GET['id'])) {
                                $contents['body'] = 'Запись успешно удалена!';
                            } else {
                                $contents['body'] = 'Произошла ошибка при удалении записи!';
                            }
                        }
                        break;
                    default:
                        $action = 'home';
                }
            } elseif (isset($_POST['action'])) {
                $action = $_POST['action'];
                switch ($action) {
                    case 'savePage':
                        if (isset($_POST['id']) && ($_POST['id'] > 0)) {
                            $contents['body'] = \Page::update($_POST);
                        } else {
                            $contents['body'] = \Page::save($_POST);
                        }
                        break;
                    case 'deletePage':
                        if (isset($_POST['ajax']) && isset($_POST['id'])) {
                            $result = \Page::deleteAjax($_POST['id']);
                            echo json_encode($result);
                            exit();
                        }
                        break;
                    default:
                        $action = 'home';
                }
            }
        } elseif (isset($_POST['doLogin']) && \User::checkPassword($_POST['email'], $_POST['passwd'])) {
            \User::doAuthorize();
        }


        $this->output['contents'] = $contents;
        $this->output['action'] = $action;
        $this->output['is_authorized'] = \User::isAuthorized();
    }
}