<?php
namespace Controllers;

class Comment extends \Controller
{
    public function run()
    {
        parent::run();

        $username = \Request::getPost('username');
        $commentText = \Request::getPost('comment');
        $msg = \Comment::save($username, $commentText);

        $isAjax = (\Request::getPost('ajax') == '' ? false : true);
        if ($isAjax) {
            $comment = new \Comment();
            $result = array(
                'status' => 'ok',
                'msg' => $msg,
                );
            echo json_encode($result);
            exit();
        } else {
            $contents['msg'] = $msg;
            $contents['comments'] = \Comment::fetchAll();
        }

        $this->output['contents'] = $contents;
        $this->output['action'] = 'comment';

    }
}