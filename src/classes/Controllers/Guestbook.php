<?php
namespace Controllers;

class Guestbook extends \Controller
{
    protected $tpl = 'gbook.tpl';
    public function run()
    {
        parent::run();

        $page = (int)\Request::parseUrl(1);
        $perPage = 5;
        $comment = new \Comment();
        $this->output['comments'] = $comment->get($page, $perPage);

        $cnt = $comment->count();

        $this->output['curPage'] = $page;
        $this->output['pages'] = floor($cnt / $perPage) + 1;

        $this->output['action'] = 'guestbook';

    }
}