<?php
namespace Controllers;

class Home extends \Controller
{
    public function run()
    {
        parent::run();
        $id = \Request::parseUrl(1);
        $contents = array();

        $this->output['contents'] = $contents;
        $this->output['action'] = 'home';
    }
}