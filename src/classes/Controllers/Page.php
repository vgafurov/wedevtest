<?php
namespace Controllers;

class Page extends \Controller
{
    protected $tpl = 'page.tpl';
    public function run()
    {
        parent::run();
        $id = \Request::parseUrl(1);
        $contents = array();
        if (isset($id)) {
            $contents['row'] = \Page::show($id);
        }
        $this->output['contents'] = $contents;
        $this->output['action'] = 'page';
    }
}