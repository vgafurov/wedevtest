<?php

class Database
{
    public static $instance = null;

    static public function getConnection()
    {
        if (null === self::$instance) {
            try {
                require 'config/database.inc.php';
                self::$instance = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['db_name'], $db['user'], $db['password']);
                self::$instance->query("SET NAMES 'utf8'");
            } catch (PDOException $e) {
                echo $e->getMessage();
                exit();
            }
        }
        return self::$instance;
    }
}