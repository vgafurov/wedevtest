<?php

class Page
{
    static public function fetchAll()
    {
        $dbh = Database::getConnection();

        $sql = "SELECT * FROM `tblPages`";
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $rows;

    }

    static public function edit($id)
    {
        $dbh = Database::getConnection();

        $sql = "SELECT * FROM `tblPages` WHERE `id` = :id";
        $placeholders = array(':id' => $id);

        $stmt = $dbh->prepare($sql);
        $stmt->execute($placeholders);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row;
    }

    static public function update($arr)
    {
        $dbh = Database::getConnection();

        $sql = "UPDATE `tblPages` SET `title` = :title, `body` = :body WHERE `id` = :id";
        $placeholders = array(
            ':title' => $arr['title'],
            ':body' => $arr['body'],
            ':id' => $arr['id'],
        );

        $stmt = $dbh->prepare($sql);
        if ($stmt->execute($placeholders)) {
            $html = 'Запись обновлена';
        }

        return $html;
    }

    static public function save($arr)
    {
        $dbh = Database::getConnection();

        $sql = "INSERT INTO `tblPages` SET `title` = :title, `body` = :body";
        $placeholders = array(
            ':title' => $arr['title'],
            ':body' => $arr['body'],
        );

        $stmt = $dbh->prepare($sql);
        if ($stmt->execute($placeholders)) {
            $html = 'Запись добавлена!';
        }

        return $html;
    }

    static public function show($id)
    {
        $dbh = Database::getConnection();

        // Отобразить соответствующую страницу
        $sql = "SELECT * FROM `tblPages` WHERE `id` = :id";
        $placeholders = array(':id' => $id);

        $stmt = $dbh->prepare($sql);
        $stmt->execute($placeholders);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row;
    }

    static public function getMenu()
    {
        $dbh = Database::getConnection();

        $sql = "SELECT * from `tblPages`";
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }

    static public function delete($id)
    {
        $dbh = Database::getConnection();

        $sql = "DELETE FROM `tblPages` WHERE `id` = :id LIMIT 1";
        $placeholders = array(':id' => $id);
        $stmt = $dbh->prepare($sql);

        if ($stmt->execute($placeholders)) {
            return true;
        } else {
            return false;
        }
    }
  
    static public function deleteAjax($id)
    {
        if (self::delete($id)) {
            return array('status' => 'ok', 'msg' => 'Успешно удалено!');
        } else {
            return array('status' => 'error', 'msg' => 'Ошибка при удалении.');
        }
    }
}