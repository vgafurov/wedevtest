<?php

class Request
{
    public static function getPost($arg = '')
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST)) {
            $ret = $_POST;
            if ($arg === '') {
                return $ret;
            } elseif (array_key_exists($arg, $ret)) {
                return $ret[$arg];
            } else {
                return null;
            }
        } else { 
            return null;
        }
    }
    
    public static function getGet($arg = '')
    {
        $ret = $_GET;
        if ($arg === '') {
            return $ret;
        } elseif (array_key_exists($arg, $ret)) {
            return $ret[$arg];
        } else {
            return null;
        }
    }
    
    public static function parseUrl($index = null)
    {
        $url = $_SERVER['REQUEST_URI'];
        $urlArray = array();
        $urlArray = explode("/", $url);
        array_shift($urlArray);

        if (count($urlArray) > 0) {
            if ($index !== null && isset($urlArray[$index])) {
                return $urlArray[$index];
            } else {
                return null;
            }

        }
    }

    public static function validate($val, $type = 'string', $range = '')
    {

    }

    public static function getLanguage()
    {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        return $lang;
    }
}
