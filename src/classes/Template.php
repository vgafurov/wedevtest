<?php

class Template
{
    public static $instance = null;

    static public function getInstance($tpl = 'default')
    {
        if (null === self::$instance) {
            self::$instance = new \Smarty();

            // Настройки Smarty
            self::$instance->setTemplateDir('templates/' . $tpl . '/');
            self::$instance->setCompileDir('temp/smarty/templates_c/');
            self::$instance->setCacheDir('temp/smarty/cache/');
        }
        return self::$instance;
    }
}