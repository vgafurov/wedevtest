<?php

class User
{

    static public function checkPassword($email, $pass)
    {
        $dbh = Database::getConnection();
        $sql = "SELECT * FROM `tblAdmins` WHERE `email` = :email";
        $placeholders = array(':email' => $email);

        $stmt = $dbh->prepare($sql);
        $stmt->execute($placeholders);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (
            (count($row) > 0)
            && (password_verify($pass, $row['passwd']))
            ) {
            return true;
        } else {
            return false;
        }
    }

    static public function isAuthorized()
    {
        return isset($_SESSION['loggedIn']);
    }

    static public function doLogout()
    {
        session_destroy();
        header('Location: /');
    }

    static public function doAuthorize()
    {
        $_SESSION['loggedIn'] = true;
        header('Location: /administrator/');
    }

}