<?php

$routes = array(
    '' => 'Controllers\Home',
    'page' => 'Controllers\Page',
    'guestbook' => 'Controllers\Guestbook',
    'comment' => 'Controllers\Comment',
    'administrator' => 'Controllers\Administrator',
    );