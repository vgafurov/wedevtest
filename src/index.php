<?php

include_once('vendor/autoload.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

include('config/routes.php');

$action = Request::parseUrl(0);
$output = array();
if (isset($routes[$action])) {
    $ctl = new $routes[$action]();
} else {
    $ctl = new Controller/Home();
}

$ctl->run();
$ctl->render();
