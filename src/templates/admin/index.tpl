<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Корпоративный сайт</title>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/web/js/admin.js"></script>
    <link rel="stylesheet" type="text/css" href="/web/css/admin.css">
</head>
<body>
    <header>
    <h1>Админка</h1>
    <nav>
        <ul class="menu">
            {if $is_authorized}
                <li><a href="/administrator/?action=listPages">Посмотреть страницы</a></li>
                <li><a href="/administrator/?action=addPage">Добавить страницу</a></li>
                <li><a href="/administrator/?action=logout">Выйти</a></li>
            {else}
                <li><a href="/administrator/?action=help">Помощь</a></li>
            {/if}
        </ul>
    </nav>
    </header>
    <main>
        {if $is_authorized}
            {if $action == 'home'}
                <h1>Часть сайта для авторизованных пользователей</h1>
                <p><a href="/administrator/?action=logout">Выйти</a></p>
            {elseif $action == 'addPage'}
            <form name="frmEditPage" action="/administrator/" method="post">
                <input type="hidden" name="action" value="savePage">
                <input type="hidden" name="id" value="0">

                <div>
                <label for="title">Название страницы</label>
                <input name="title" value="">
                </div>

                <div>
                <label for="body">Текст страницы</label>
                <textarea name="body"></textarea>
                </div>

                <div>
                <button>Сохранить!</button>
                </div>

            </form>
            {elseif $action == 'editPage'}
                {if $contents.row != null}
                <form name="frmEditPage" action="/administrator/" method="post">
                    <input type="hidden" name="action" value="savePage">
                    <input type="hidden" name="id" value="{$contents.row.id}">

                    <div>
                    <label for="title">Название страницы</label>
                    <input name="title" value="{$contents.row.title|htmlspecialchars}">
                    </div>

                    <div>
                    <label for="body">Текст страницы</label>
                    <textarea name="body">{$contents.row.body|htmlspecialchars}</textarea>
                    </div>

                    <div>
                    <button>Сохранить!</button>
                    </div>

                </form>
                {/if}
            {elseif $action == 'listPages'}
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Текст</th>
                        <th>Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$contents.rows item=row}
                    <tr>
                        <td><a href="/administrator/?action=editPage&id={$row.id}">{$row.id}</a></td>
                        <td>{$row.title|htmlspecialchars}</td>
                        <td>{$row.body|htmlspecialchars}</td>
                        <td><a href="/administrator/?action=deletePage&id={$row.id|htmlspecialchars}" class="delete" data-id="{$row.id|htmlspecialchars}">Удалить</a></td>
                    </tr>
                    {/foreach}

                </tbody>
            <table>
            {else}
                {$contents.body}
            {/if}
        {else}
        <form name="frmLogin" action="/administrator/" method="post">
            <input type="text" name="email" placeholder="Ваш email">
            <input type="password" name="passwd" placeholder="Ваш пароль">
            <button name="doLogin">Войти!</button>
        </form>
        {/if}
    </main>
</body>
</html>