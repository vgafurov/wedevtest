{foreach from=$comments item=comment}
    <div>
        «{$comment.username|htmlspecialchars}» написал:
        <p>{$comment.comment|htmlspecialchars}</p>
    </div>
{/foreach}

{if $pages > 1}
	<ul class="paginator">
	{* Первая/прошлая страница *}
	{if $curPage == 1}
		<li class="disabled">«</li>
	{else}
		<li><a href="/guestbook/{$curPage - 1}">«</a></li>
	{/if}

	{for $index=1 to $pages}
		{if $index == $curPage}
			<li class="active">{$index}</li>
		{else}
			<li><a href="/guestbook/{$index}">{$index}</a></li>
		{/if}
	{/for}

	{* Следующая/последняя страница *}
	{if $curPage == $pages}
		<li class="disabled">»</li>
	{else}
		<li><a href="/guestbook/{$curPage + 1}">»</a></li>
	{/if}

	</ul>
{/if}