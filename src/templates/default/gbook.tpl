{extends file="index.tpl"}

{block name="title" prepend}Гостевая книга - {/block}

{block name="contents"}

<h2>Гостевая книга</h2>
<div class="message"></div>
<form name="frm_comment" action="/" method="post">
    <label for="username">Имя</label> <input type="text" name="username" value="" id="username"> <br />
    <label for="comment">Текст комментария</label> <textarea name="comment" id="comment"></textarea> <br />
    <button name="doComment" id="doComment">Отправить</button>
</form>

<div id="comments">
    {include file="comments.tpl"}
</div>
{/block}


