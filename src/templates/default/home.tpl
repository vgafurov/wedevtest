<div class="features">
    <div class="single-feature">
        <img src="/web/images/features/1.png">
        <div class="text">
            <h4>TAX STRATEGIES</h4>
            <p>Eiusmod temporincididunt ut.</p>
            <a href="#">Read more →</a>
        </div>
    </div>
    <div class="single-feature">
        <img src="/web/images/features/2.png">
        <div class="text">
            <h4>BUSINESS PLANNING</h4>
            <p>Eiusmod temporincididunt ut labore.</p>
            <a href="#">Read more →</a>
        </div>
    </div>
    <div class="single-feature">
        <img src="/web/images/features/3.png">
        <div class="text">
            <h4>BUSINESS SOLUTIONS</h4>
            <p>Do eiusmod temporincididunt ut.</p>
            <a href="#">Read more →</a>
        </div>
    </div>
</div>
<div class="welcome-message">
    <h2>WELCOME !</h2>
    <h3>OUR COMPANY IS COMPRISED OF A TEAM OF EXPERTS IN FINANCIAL AND BUSINESS INDUSTRY</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
    <p>et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
    <p>ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eufugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
    <a href="#" class="button">Join Us</a>
</div>
<div class="features">
    <div class="single-feature-2">
        <h3>INNOVATIONS</h3>
        <img src="/web/images/features-2/1.jpg">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <a href="#" class="button">MORE</a>
    </div>
    <div class="single-feature-2">
        <h3>STRATEGY</h3>
        <img src="/web/images/features-2/2.jpg">
        <ul>
        <li><a href="#">Lorem ipsum dolor sit amet conseur</a></li>
        <li><a href="#">Adipisicing elit, sed do eiusmod temp</a></li>
        <li><a href="#">Encididunt ut labore massa estum</a></li>
        <li><a href="#">Ut enim ad minim veniamnostrud</a></li>
        </ul>
        <a href="#" class="button">MORE</a>
    </div>
    <div class="single-feature-2">
        <h3>RESEARCH</h3>
        <img src="/web/images/features-2/3.jpg">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <a href="#" class="button">MORE</a>
    </div>
</div>
<div class="mini-articles">
    <div class="single-article">
        <h3>OUR ADVANTAGES</h3>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</p>
        <ul>
            <li><a href="#">Customized Health and Safety Manual & Handbooks</a></li>
            <li><a href="#">Workwell Evaluation Assistance</a></li>
            <li><a href="#">Health and Safety Training</a></li>
        </ul>
        <a href="#" class="button">MORE</a>
    </div>
    <div class="single-article">
        <h3>OUR PHILOSOPHY</h3>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</p>

        <p>Corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>

        <p>As dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
        <a href="#" class="button">MORE</a>
    </div>
</div>