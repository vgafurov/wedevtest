<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{block name="title"}Корпоративный сайт{/block}</title>

    <link rel="stylesheet" type="text/css" href="/web/css/style.css">
    
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/web/js/script.js"></script>
</head>
<body>

    <div class="wrapper">
        <div class="header">
            <a href="#"><img src="/web/images/logo.png"></a>
        </div>
        <div class="top-menu">
            <ul>
                {include file='menu.tpl'}
            </ul>
        </div>
        <div class="slider">
            <img src="/web/images/slider.png">
        </div>

        <div class="contents">
        {block name="contents"}
            {include file="home.tpl"}
        {/block}
        </div>
        <div class="pre-footer">
            <div class="sub-prefooter-1">
                <h4>ABOUT US</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>

                <p>Et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in.</p>
            </div>
            <div class="sub-prefooter-2">
                <h4>OUR LOCATION</h4>
                <p>9870 St Vincent Place,<br>
                Glasgow, DC 45 Fr 45.</p>

                <p>Телефон: +7 901 234 56 78<br>
                Email: <a href="mailto:info@domain.tld">info@domain.tld</a></p>
            </div>
            <div class="sub-prefooter-3">
                <h4>NAVIGATION</h4>
                <ul>
                    {include file='menu.tpl'}
                </ul>
            </div>
        </div>
        <div class="footer">
            <div class="social">
                <img src="/web/images/socials.png">
            </div>
            <div class="copyright">
                <h6>DYNAMIC</h6>
                <p>© 2013 |PRIVACY POLICY</p>
            </div>
            <div class="partners">
                <img src="/web/images/partners/1.png">
                <img src="/web/images/partners/2.png">
                <img src="/web/images/partners/3.png">
                <img src="/web/images/partners/4.png">
            </div>
        </div>
    </div>
</body>
</html>