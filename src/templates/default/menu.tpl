            <li><a href="/">Главная</a></li>
            {if $menu_items|@count > 0}
                {foreach from=$menu_items item=row}
                    <li><a href="/page/{$row.id|htmlspecialchars}">{$row.title|htmlspecialchars}</a></li>
                {/foreach}
            {/if}
            <li><a href="/guestbook/">Гостевая</a></li>