{extends file="index.tpl"}

{block name="title" prepend}{$contents.row.title|htmlspecialchars} - {/block}

{block name="contents"}

<h2>{$contents.row.title|htmlspecialchars}</h2>
<p>{$contents.row.body|nl2br}</p>

{/block}
