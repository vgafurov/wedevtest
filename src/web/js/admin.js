$(document).ready(function () {
    $('.delete').on('click', function (e) {
        e.preventDefault();
        if (confirm('Вы уверены, что хотите удалить?')) {
            var $tr = $(this).parents('tr'),
                idToDelete = $(this).attr('data-id');

            $.ajax({
                type: 'post',
                url: 'admin.php',
                data: {
                    id: idToDelete,
                    ajax: 1,
                    action: 'deletePage'
                },
                success: function (json) {
                    if (json.status == 'ok') {
                        $tr.remove();
                    } else {
                        alert('Ошибка на сервере!');
                    }
                },
                dataType: 'json',
                error: function () {
                    alert('Произошла ошибка при отправке запроса!');
                }
            });
        }
    });
});