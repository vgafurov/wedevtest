
$(document).ready(function () {

    $('#doComment').click(function (e) {
        e.preventDefault();
        $('#comments').prepend('«' + $('#username').val() + '» написал' + '<p>' + $('#comment').val() + '</p>');
        
        var data = {
            doComment: 1,
            ajax: '1',
            username: $('#username').val(),
            comment: $('#comment').val(),
        };
        $.ajax({
            url: '/comment/',
            type: 'post',
            data: data,
            success: function (json) {
                if (json.status == 'ok') {
                    $('#username').val('');
                    $('#comment').val('');
                    $('.message').html(json.msg);
                } else {
                    alert(json.msg);
                }
            },
            dataType: 'json',
            error: function () {
                alert('Произошла ошибка при отправлении комментария!');
            },
        });
    });

});
